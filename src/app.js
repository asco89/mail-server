const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header('Access-Control-Expose-Headers', 'ili_ticket');
	next();
});

app.options("/*", (req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	res.header('Access-Control-Expose-Headers', 'ili_ticket');

	res.send(200);
});

app.post('/mail', (req, res) => {
    const {mailOptions, transporterOptions} = req.body;

    const transporter = nodemailer.createTransport(transporterOptions);
    transporter.sendMail(mailOptions, (err) => {
        if (!err) {
            console.log('sent', mailOptions);
        } else {
            console.log(err);
        }
    });

    res.sendStatus(200);
});

const port = process.env.PORT || 8080;

app.listen(port, () => console.log(`Listening on port ${port}`))
